from flask import Blueprint
from flask_restful import Api

import parking_service


v1_bp = Blueprint('api_v1', __name__)

v1_api = Api(v1_bp)
v1_api.add_resource(
    parking_service.gateways.resources.v1.ParkingList,
    '/parking/'
)
v1_api.add_resource(
    parking_service.gateways.resources.v1.Parking,
    '/parking/<int:location>'
)
