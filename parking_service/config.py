from environs import Env


env = Env()


class Config:
    SECRET_KEY = env.str('SECRET_KEY')

    DEBUG = env.bool('DEBUG', default=False)

    PARKING_SIZE = env.int(
        'PARKING_SIZE',
        default=2,
        validate=lambda x: x > 0
    )

