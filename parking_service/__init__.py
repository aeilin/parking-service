from . import domains
from . import repositories
from . import services
from . import gateways
from . import config


def create_app():
    from parking_service import blueprints
    from parking_service import app_factory
    BLUEPRINTS = (
        dict(blueprint=blueprints.v1_bp, url_prefix='/api/v1'),
    )

    app = app_factory.create_app(blueprints=BLUEPRINTS, config=config.Config)
    return app
