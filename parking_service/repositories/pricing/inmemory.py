import inject

from .base import PricingRepositoryBase
from parking_service.repositories import exceptions


class PricingInMemoryRepository(PricingRepositoryBase):
    @inject.autoparams()
    def __init__(self, tariffs):
        self._tariffs = tariffs.copy()

    def get_price(self, tariff):
        try:
            price = self._tariffs[tariff]
        except KeyError:
            raise exceptions.PriceNotFoundError(f'there is no price set for "{tariff}" tariff')
        except Exception as e:
            raise exceptions.InternalError from e

        return price
