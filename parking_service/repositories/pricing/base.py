from abc import ABC, abstractmethod


class PricingRepositoryBase(ABC):
    @abstractmethod
    def get_price(self, tariff):
        pass
