from abc import ABC, abstractmethod


class SpaceRepositoryBase(ABC):
    @abstractmethod
    def acquire_space(self):
        pass

    @abstractmethod
    def free_space(self, space):
        pass
