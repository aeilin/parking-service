from typing import Deque

from .base import SpaceRepositoryBase
from parking_service.repositories import exceptions


class SpaceInMemoryRepository(SpaceRepositoryBase):
    def __init__(self, storage: Deque):
        self._free_spaces = storage

    def acquire_space(self):
        try:
            space = self._free_spaces.pop()
        except IndexError:
            raise exceptions.NoFreeLocationError
        except Exception as e:
            raise exceptions.InternalError from e

        return space

    def free_space(self, space):
        self._free_spaces.append(space)
