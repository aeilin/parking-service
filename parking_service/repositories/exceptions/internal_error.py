from . import RepositoryError


class InternalError(RepositoryError):
    pass
