from . import RepositoryError


class PriceNotFoundError(RepositoryError):
    pass
