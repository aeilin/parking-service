from . import RepositoryError


class NoFreeLocationError(RepositoryError):
    pass
