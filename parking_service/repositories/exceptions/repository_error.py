from parking_service import exceptions


class RepositoryError(exceptions.ParkingServiceError):
    pass
