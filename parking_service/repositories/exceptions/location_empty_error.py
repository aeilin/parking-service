from . import RepositoryError


class LocationEmptyError(RepositoryError):
    pass
