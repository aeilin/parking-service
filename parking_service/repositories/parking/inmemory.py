from typing import Dict

from .base import ParkingRepositoryBase
from parking_service.repositories import exceptions


class ParkingInMemoryRepository(ParkingRepositoryBase):
    def __init__(self, storage: Dict):
        self._parking = storage

    def add(self, location, parking_entity):
        self._parking[location] = parking_entity

    def get(self, location):
        try:
            entity = self._parking[location]
        except KeyError:
            raise exceptions.LocationEmptyError(f'location #{location} are empty')
        except Exception as e:
            raise exceptions.InternalError from e

        return entity
    
    def delete(self, location):
        try:
            del self._parking[location]
        except KeyError:
            raise exceptions.LocationEmptyError(f'location #{location} are empty')
        except Exception as e:
            raise exceptions.InternalError from e

    def list_all(self):
        return list(
            self._parking.values()
        )
