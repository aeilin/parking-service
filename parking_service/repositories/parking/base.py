from abc import ABC, abstractmethod


class ParkingRepositoryBase(ABC):
    @abstractmethod
    def add(self, location, parking_entity):
        pass

    @abstractmethod
    def get(self, location):
        pass

    @abstractmethod
    def delete(self, location):
        pass

    @abstractmethod
    def list_all(self):
        pass
