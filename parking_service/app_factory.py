import logging

import inject
from flask import Flask

from parking_service import extensions
from .injector import configure_injector


def configure_app(app, config):
    if config:
        app.config.from_object(config)


def configure_extensions(app):
    # marshmallow
    extensions.ma.init_app(app)


def configure_blueprints(app, blueprints):
    for blueprint in blueprints:
        app.register_blueprint(**blueprint)


def configure_logging(app, config):
    # disable default flask logging
    if not config.DEBUG:
        app.logger.disabled = True
        default_flask_logger = logging.getLogger('werkzeug')
        default_flask_logger.disabled = True


def create_app(blueprints, config):
    app = Flask(__name__)

    configure_app(app, config)
    configure_logging(app, config)
    configure_blueprints(app, blueprints)
    configure_extensions(app)

    inject.clear_and_configure(configure_injector)

    return app
