from math import ceil
from abc import ABC, abstractmethod

from . import Tariff
from parking_service import exceptions


class UnknonwParkingStrategyError(exceptions.ParkingServiceError):
    pass


DEFAULT_FREE_OF_CHARGE_MINUTES = 15


class ParkingStrategy(ABC):
    def __init__(self, price, free_of_charge_minutes=DEFAULT_FREE_OF_CHARGE_MINUTES):
        self.__price = price
        self.__free_of_charge_minutes = free_of_charge_minutes

    @abstractmethod
    def _calculate_period(self, seconds):
        pass

    def _is_free_of_charge(self, seconds):
        return seconds <= self.__free_of_charge_minutes * 60

    def calculate_fee(self, start_time, end_time):
        dd = end_time - start_time
        seconds = dd.total_seconds()
        if self._is_free_of_charge(seconds):
            return 0

        period = self._calculate_period(seconds)
        fee = ceil(period) * self.__price

        return fee


class ParkingHourlyStrategy(ParkingStrategy):
    def _calculate_period(self, seconds):
        hours = seconds / (60 ** 2)

        return hours


class ParkingDailyStrategy(ParkingStrategy):
    def _calculate_period(self, seconds):
        days = seconds / (60**2 * 24)

        return days


def parking_stratetgy_fabric(*, tariff):
    tariff_strategy_map = {
        Tariff.HOURLY: ParkingHourlyStrategy,
        Tariff.DAILY: ParkingDailyStrategy
    }

    try:
        return tariff_strategy_map[tariff]
    except:
        raise UnknonwParkingStrategyError
