class ParkingEntity:
    def __init__(self, *, car, tariff, start_time, location):
        self.__car = car
        self.__tariff = tariff
        self.__location = location
        self.__start_time = start_time
        self.__end_time = None

        self.__fee = None

        self.__strategy = None

    def calculate_fee(self):
        self.__fee = self.strategy.calculate_fee(self.__start_time, self.__end_time)
        return self.fee

    @property
    def car(self):
        return self.__car

    @property
    def tariff(self):
        return self.__tariff

    @property
    def location(self):
        return self.__location

    @property
    def fee(self):
        return self.__fee

    @property
    def start_time(self):
        return self.__start_time

    @property
    def end_time(self):
        return self.__end_time

    @end_time.setter
    def end_time(self, end_time):
        self.__end_time = end_time

    @property
    def strategy(self):
        return self.__strategy

    @strategy.setter
    def strategy(self, strategy):
        self.__strategy = strategy
