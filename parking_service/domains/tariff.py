import enum

@enum.unique
class Tariff(enum.Enum):
    HOURLY = 'hourly'
    DAILY = 'daily'
