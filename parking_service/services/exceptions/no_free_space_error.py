from . import ServiceError


class NoFreeSpaceError(ServiceError):
    pass
