from parking_service import exceptions


class ServiceError(exceptions.ParkingServiceError):
    pass
