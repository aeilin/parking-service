from . import ServiceError


class InternalError(ServiceError):
    pass
