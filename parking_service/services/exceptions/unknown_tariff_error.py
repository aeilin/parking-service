from . import ServiceError


class UnknownTariffError(ServiceError):
    pass
