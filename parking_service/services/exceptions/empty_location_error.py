from . import ServiceError


class EmptyLocationError(ServiceError):
    pass
