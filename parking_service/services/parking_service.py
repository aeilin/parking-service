import logging
from datetime import datetime

import inject

from parking_service import (
    domains,
    repositories
)
from parking_service.services import exceptions


logger = logging.getLogger(__name__)


class ParkingService:
    @inject.autoparams()
    def __init__(
        self,
        *,
        parking_repositrory: repositories.parking.ParkingRepositoryBase,
        pricing_repository: repositories.pricing.PricingRepositoryBase,
        space_repository: repositories.space.SpaceRepositoryBase,
    ):
        self.__parking_repository = parking_repositrory
        self.__pricing_repository = pricing_repository
        self.__space_repository = space_repository

    def park(self, car, tariff):
        try:
            location = self.__space_repository.acquire_space()
        except repositories.exceptions.NoFreeLocationError:
            raise exceptions.NoFreeSpaceError
        except:
            logger.error("aqcuiring space error", exc_info=True)
            raise exceptions.InternalError

        start_time = datetime.now()
        parking_entity = domains.ParkingEntity(
            car=car,
            tariff=tariff,
            start_time=start_time,
            location=location
        )

        try:
            self.__parking_repository.add(location, parking_entity)
        except:
            logger.error("parking add error. car: %s tariff: %s", car, tariff, exc_info=True)
            self.__space_repository.free_space(location)
            raise exceptions.InternalError

        return parking_entity

    def unpark(self, location):
        try:
            parking_entity = self.__parking_repository.get(location)
        except repositories.exceptions.LocationEmptyError:
            raise exceptions.EmptyLocationError(f'{location} are empty')
        except:
            logger.error("can't get parking entity. location: %s", location, exc_info=True)
            raise exceptions.InternalError

        try:
            PricingStrategy = domains.parking_stratetgy_fabric(tariff=parking_entity.tariff)
        except domains.parking_strategies.UnknonwParkingStrategyError:
            logger.error("unknown parking strategy. tariff: %s", parking_entity.tariff, exc_info=True)
            raise exceptions.UnknownTariffError(f'unknown tariff {parking_entity.tariff}')

        try:
            price = self.__pricing_repository.get_price(parking_entity.tariff)
        except repositories.exceptions.PriceNotFoundError:
            logger.error("price not found. tariff: %s", parking_entity.tariff, exc_info=True)
            raise exceptions.UnknownTariffError(f'price not found for tariff {parking_entity.tariff}')
        except:
            logger.error("can't get price for tariff. tariff: %s", parking_entity.tariff, exc_info=True)
            raise exceptions.InternalError

        parking_entity.strategy = PricingStrategy(price)
        parking_entity.end_time = datetime.now()
        parking_entity.calculate_fee()

        self.__space_repository.free_space(location)
        try:
            self.__parking_repository.delete(location)
        except repositories.exceptions.LocationEmptyError:
            pass
        except:
            logger.error("can't remove parking. location: %s", location, exc_info=True)
            raise exceptions.InternalError

        return parking_entity

    def list_all(self):
        return self.__parking_repository.list_all()
