from collections import deque

import inject

from parking_service import (
    config,
    domains,
    repositories,
    services,
)


@inject.autoparams()
def provide_PricingInMemoryRepository(configs: config.Config):
    tariffs = {
        domains.Tariff.DAILY: 20,
        domains.Tariff.HOURLY: 2,
    }

    return repositories.pricing.PricingInMemoryRepository(tariffs)

@inject.autoparams()
def provide_ParkingInMemoryRepository(configs: config.Config):
    storage = dict()
    return repositories.parking.ParkingInMemoryRepository(storage)

@inject.autoparams()
def provide_SpaceInMemoryRepository(configs: config.Config):
    storage = deque(
        range(configs.PARKING_SIZE),
        maxlen=configs.PARKING_SIZE
    )
    return repositories.space.SpaceInMemoryRepository(storage)


def configure_injector(binder):
    binder.bind(config.Config, config.Config)

    binder.bind_to_constructor(
        repositories.pricing.PricingRepositoryBase,
        provide_PricingInMemoryRepository,
    )
    binder.bind_to_constructor(
        repositories.parking.ParkingRepositoryBase,
        provide_ParkingInMemoryRepository
    )
    binder.bind_to_constructor(
        repositories.space.SpaceRepositoryBase,
        provide_SpaceInMemoryRepository
    )
    binder.bind_to_constructor(
        services.ParkingService,
        services.ParkingService
    )
