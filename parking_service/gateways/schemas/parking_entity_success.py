from parking_service.extensions import ma

from .parking_entity import ParkingEntity


class ParkingEntitySuccess(ma.Schema):
    status = ma.String(required=True, default='success')
    data = ma.Nested(ParkingEntity(), required=True)
