from parking_service.extensions import ma

from .parking_entity import ParkingEntity


class ParkingEntitiesListSuccess(ma.Schema):
    status = ma.String(required=True, default='success')
    data = ma.Nested(
        ParkingEntity(
            exclude=('fee', 'end_time')
        ),
        many=True,
        required=True,
    )
