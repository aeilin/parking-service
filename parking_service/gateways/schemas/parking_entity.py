import marshmallow_enum as ma_enum

from parking_service import domains
from parking_service.extensions import ma


class ParkingEntity(ma.Schema):
    car = ma.String(
        required=True
    )

    tariff = ma_enum.EnumField(
        domains.Tariff,
        by_value=True,
        required=True
    )

    location = ma.Int(
        dump_only=True
    )

    fee = ma.Float(
        dump_only=True
    )

    start_time = ma.DateTime(
        dump_only=True
    )
    end_time = ma.DateTime(
        dump_only=True
    )
