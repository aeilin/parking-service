from parking_service.extensions import ma


class Error(ma.Schema):
    status = ma.String(required=True)
    title = ma.String(required=True)
    details = ma.String(required=False)
