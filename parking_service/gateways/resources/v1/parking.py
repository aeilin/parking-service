import inject
from flask_restful import Resource

from parking_service.gateways import schemas
from parking_service.services import (
    exceptions,
    ParkingService
)


parking_schema_success = schemas.ParkingEntitySuccess()
error_schema = schemas.Error()


class Parking(Resource):
    @inject.autoparams()
    def __init__(self, service: ParkingService):
        self.__parking_service = service

    def delete(self, location: int):
        response = {'status': 'success'}
        code = 200

        try:
            entity = self.__parking_service.unpark(location)
        except exceptions.UnknownTariffError as e:
            response = {'status': 'error', 'title': 'Internal Error', 'details': str(e)}
            code = 500
            schema = error_schema
        except exceptions.EmptyLocationError as e:
            response = {'status': 'error', 'title': 'Not Found', 'details': str(e)}
            code = 404
            schema = error_schema
        except exceptions.InternalError as e:
            response = {'status': 'error', 'title': 'Internal Error', 'details': str(e)}
            code = 500
            schema = error_schema
        except:
            response = {'status': 'error', 'title': 'Internal Error'}
            code = 500
            schema = error_schema
        else:
            response['data'] = entity
            schema = parking_schema_success

        return schema.dump(response), code
