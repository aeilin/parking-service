import inject
from flask import request
from marshmallow import exceptions as ma_exceptions
from flask_restful import Resource

from parking_service.gateways import schemas
from parking_service.services import (
    exceptions,
    ParkingService
)


parking_schema_input = schemas.ParkingEntity(only=('car', 'tariff'))
parking_schema_success = schemas.ParkingEntitySuccess(exclude=('data.fee', 'data.end_time'))
parking_list_schema_success = schemas.ParkingEntitiesListSuccess()
error_schema = schemas.Error()


class ParkingList(Resource):
    @inject.autoparams()
    def __init__(self, service: ParkingService):
        self.__parking_service = service

    def get(self):
        response = {'status': 'success'}
        code = 200

        try:
            cars = self.__parking_service.list_all()
        except:
            response = {'status': 'error', 'title': 'Internal Error'}
            code = 500
            schema = error_schema
        else:
            response['data'] = cars
            schema = parking_list_schema_success

        return schema.dump(response), code

    def post(self):
        response = {'status': 'success'}
        code = 201

        try:
            request_obj = parking_schema_input.load(request.form)
        except ma_exceptions.ValidationError as e:
            response = {'status': 'error', 'title': 'Badly Formed', 'details': e.normalized_messages()}
            code = 400

            return error_schema.dump(response), code

        try:
            entity = self.__parking_service.park(request_obj['car'], request_obj['tariff'])
        except exceptions.NoFreeSpaceError:
            response = {'status': 'error', 'title': 'No Free Space', 'details': 'no free space available'}
            code = 200
            schema = error_schema
        except exceptions.InternalError as e:
            response = {'status': 'error', 'title': 'Internal Error', 'details': str(e)}
            code = 500
            schema = error_schema
        except:
            msg = {'status': 'error', 'title': 'Internal Error'}
            code = 500
            schema = error_schema
        else:
            response['data'] = entity
            schema = parking_schema_success

        return schema.dump(response), code
