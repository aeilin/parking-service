import os
from setuptools import setup, find_packages
from pathlib import Path


REQUIRED = [
    'Flask==1.1.2',
    'Flask-RESTful==0.3.8',

    'flask-marshmallow==0.11.0',
    'marshmallow==3.5.1',
    'marshmallow-enum==1.5.1',

    'Inject==4.1.2',

    'environs==7.4.0',
]

here = os.path.abspath(os.path.dirname(__file__))

about = dict()
with open(os.path.join(here, 'parking_service', '__version__.py'), 'r') as f:
    exec(f.read(), about)


setup(
    name=about['__title__'],
    version=about['__version__'],
    packages=find_packages(),
    url=about['__url__'],
    install_requires=REQUIRED,
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        'Framework :: Flask',
        'Operating System :: OS Independent',
        "Programming Language :: Python :: 3.7",
        "Natural Language :: English",
    ]
)
