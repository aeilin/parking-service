SHELL := /bin/bash

PY_VERSION ?= 3.7
PIP ?= pip$(PY_VERSION)
PYVENV ?= python$(PY_VERSION) -m venv
REQUIREMENTS ?= requirements.txt

mkfile_path = $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT_PATH ?= $(patsubst %/,%,$(dir $(mkfile_path)))
VENV_PATH ?= "$(PROJECT_PATH)/venv"

FIND_NOT_IN_VENV = find -not -path "$(VENV_PATH)/*"
ACTIVATE_VENV = . $(VENV_PATH)/bin/activate


venv: $(VENV_PATH)


$(VENV_PATH):
	$(PYVENV) $(VENV_PATH)
	$(ACTIVATE_VENV) ; \
	$(PIP) install --upgrade pip ; \
	$(PIP) install -r $(REQUIREMENTS) ; \
	deactivate ;


.PHONY: clean
clean: clean-pyc
	rm -rf *egg-info
	rm -rf dist
	rm -rf build


.PHONY: clean-pyc
clean-pyc:
	$(FIND_NOT_IN_VENV) -type d -and -not -path '__pycache__/*' \
						-name '__pycache__' -exec rm -rf {} +
	$(FIND_NOT_IN_VENV) -type f -name '*.pyc' -or -name '*.pyo' -exec rm -f {} +
