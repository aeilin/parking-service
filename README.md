[Task](#TASK)  
[Comments](#Comments)

## TASK

Please, design and automate parking lot service with three operations:
* Add a car
* Remove a car
* Lists the parked cars

Requirements:  
On entrance, the driver selects a tariff plan (e.g., hourly or daily) at the ticket machine to print the ticket.  
On exit, the station calculates the fee and prints a receipt for a driver. The first 15 minutes are free for all tariff plans.  
Must design implementation to be tolerant of tariff plan changes.  
Service should be implemented as a REST web service with three GET operations that returns a JSON content type, e.g.:  
```
http://host:port/add?car=X774HY98&tariff=hourly
{"status": "success", "car": "X774HY98", "tariff": "hourly", "location": 12, "start": "2014-10-01 14:11:45"} <-- info to print ticket for driver on entrance.

http://host:port/add?car=X774HY98&tariff=daily
{"status": "error", "No free space"} <-- info to show on display of ticket machine for driver.

http://host:port/remove?location=12
{"status": "success", "start": "2014-10-01 14:11:45", "finish": "2014-10-01 14:21:57", "location": 12, "car": "X774HY98", "fee": 0, "tariff": "hourly"} <-- info to print receipt for driver on exit.

http://host:port/list
{"status": "success", "cars" : [
    {"car": "X774HY98", "tariff": "hourly", "location": 1, "start": "2014-10-01 14:11:45"},
    {"car": "X637TT98", "tariff": "daily", "location": 2, "start": "2014-10-01 15:23:05"}
] } <-- info to parking administrator.
```
Note:  
This is just an example so feel free to change the format if required.  
Database, front-end, nor hibernation aren't required, saving the cases you could show something interesting in a reasonable time.  
Design an object-oriented abstraction on problem-solving.  
Provide good error reporting.  
Ideal implementation is not required but must work for basic cases.  


# Comments
## TODO
To fasten the first review iteration, I have skipped to implement:

* Tests (WIP)
* CI/CD
* DB
* Caching
* Different environments support (local, production, etc)
* Auth

## Notes on Task
### Design an object-oriented abstraction on problem-solving
Personally I think it's an overingenering to use strict OOP design for such a simple task. However, to demonstrate the required skills, the application was designed using the following principles:
* SOLID
* Clean Architecture
* DI + IoC containers
* OOP patterns
* PoEAA

## How To Run
1. Setup virtual environment (there is a goal `venv` in the Makefile)
2. `pip install .`
3. Setup environment variables: `SECRET_KEY` required, `PARKING_SIZE` optional
4. `flask run`

## API
### Responce Format
#### Statuses in use
200 OK
201 Created
400 Bad Request
404 Not Found
500 Internal Server Error

#### Error
Response will contain a body with the next structure
```
{'status': 'error', 'title': 'string', 'details': 'string'}
```

#### Success
Response will contain(or will not contain at all) a body with the next structure
```
{'status': 'success', 'data': object}
```


### REST Methods
**GET <uri>/api/v1/parking/**
Returns list of all currently parked cars
Statuses: 200, 500
```
curl \
  --request GET \
  --url http://127.0.0.1:5000/api/v1/parking/
```

**POST \<uri>/api/v1/parking/**
Parks car if space available
Statuses: 200(valid error), 201(created), 404, 500
_`car:string` and `tariff:{daily,hourly}` are mandatory_
```
curl \
  --request POST \
  --url http://127.0.0.1:5000/api/v1/parking/ \
  --data car=XXX777HOURLY \
  --data tariff=hourly
```
```
curl \
  --request POST \
  --url http://127.0.0.1:5000/api/v1/parking/ \
  --data car=XXX777DAILY \
  --data tariff=daily
```

**DELETE \<uri>/api/v1/parking/\<int:location>**
Unparks car and shows fee
Statuses: 200, 404, 500
```
curl \
  --request DELETE \
  --url http://127.0.0.1:5000/api/v1/parking/1
```
